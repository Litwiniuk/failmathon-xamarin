﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using Xamathon.Services;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Xamathon
{
    public class HousePage : ContentPage
    {
        public Label labelTemp;
        public Label labelWet;
        public Label labelAct;
        public Guid lastFrameId =Guid.Empty;
        Image visionImage;
        public HousePage()
        {
            var layout = new StackLayout();
            labelTemp = new Label();
            labelTemp.Text = "Temperature: ?";
            labelTemp.FontSize = 20;
            labelTemp.TextColor = Color.White;

            labelWet = new Label();
            labelWet.Text = "Humidity: ?";
            labelWet.TextColor = Color.White;
            labelWet.FontSize = 20;

            labelAct = new Label();
            labelAct.Text = "The movement of the item was not detected.";
            labelAct.FontSize = 20;
            labelAct.TextColor = Color.White;


            Label labelImage = new Label();
            BackgroundColor = Color.FromRgb(81, 126, 192);
            labelImage.Text = "Camera preview";
            labelImage.TextColor = Color.White;
            labelImage.FontSize = 14;
            labelImage.Margin = new Thickness(0,100,0,0);
            layout.Children.Add(labelTemp);
            layout.Children.Add(labelWet);
            layout.Children.Add(labelAct);
            layout.Children.Add(labelImage);
            visionImage = new Image();
            layout.Children.Add(visionImage);
            Content = layout;

            Task.Factory.StartNew(UIThread,
                CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        public async void UIThread()
        {

            for (int i = 0; i < 1000000; i++)
            {
                //IEnumerable<Xamathon.Models.Frame> frames = null;
                Xamathon.Models.CurrentPeople currentPeople = null;
                try
                {

                    IEnumerable<Xamathon.Models.SensorData> sensorData = null;

                    SensorsService s = new SensorsService();
                    sensorData = await s.GetSensorsData();
                    Models.SensorData temp = sensorData.Where(x => x.DataType == Models.SensorDataType.Temperature).FirstOrDefault();
                    Models.SensorData motion = sensorData.Where(x => x.DataType == Models.SensorDataType.InMotion).FirstOrDefault();
                    Models.SensorData wet = sensorData.Where(x => x.DataType == Models.SensorDataType.Humidity).FirstOrDefault();

                    if (temp != null)
                    {
                        var dot = temp.Data.IndexOf('.');
                        Xamarin.Forms.Device.BeginInvokeOnMainThread(() => { labelTemp.Text = "Temperature: " + temp.Data.Substring(0, dot+3); });
                    }
                    if (motion != null)
                    {
                        string value;
                        Color color;

                        if (Convert.ToBoolean(motion.Data) == true)
                        {
                            value = "ATTENTION! Item movement was detected!";
                            color = Color.Red;
                        }
                        else
                        {
                            value = "The movement of the item was not detected.";
                            color = Color.White;
                        }

                        Xamarin.Forms.Device.BeginInvokeOnMainThread(() => { labelAct.Text = value; labelAct.TextColor = color; });
                    }
                    if (wet != null)
                    {
                        var dot = temp.Data.IndexOf('.');
                        Xamarin.Forms.Device.BeginInvokeOnMainThread(() => { labelWet.Text = "Humidity: " +  wet.Data.Substring(0, dot + 3) + "%"; });
                    }

                    FrameService frameService = new FrameService();
                    Xamathon.Models.Frame frame = await frameService.GetLastFrame();

                    if (lastFrameId == Guid.Empty)
                    {
                        lastFrameId = frame.Id;
                    }
                    else
                    {
                        if (lastFrameId == frame.Id)
                        {
                            continue;
                            await Task.Delay(500);

                        }    
                    
                    }

                    Xamarin.Forms.Device.BeginInvokeOnMainThread(async () => { await visionImage.FadeTo(0, 120); visionImage.Source = Utils.Base64ToImageSource(frame.EncodedImage); await visionImage.FadeTo(1, 120); });
                    //Xamarin.Forms.Device.BeginInvokeOnMainThread(()=>{ labelTemp.Text = "WAT " + i; });                  
                    System.Diagnostics.Debug.WriteLine("WAT WAT WAT WAT ");
                    lastFrameId = frame.Id;
                    await Task.Delay(500);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString() + e.Message.ToString());
                }
            }
        }
    }
}
