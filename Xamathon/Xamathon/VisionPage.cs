﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using Xamathon.Services;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Xml.Linq;
using Xamarin.Forms;
using Xamathon.Models;
using Device = Xamarin.Forms.Device;

namespace Xamathon
{
    public class VisionPage : ContentPage
    {
        Image visionImage;
        private StackLayout layout;
        private Label labelInKnown;
        private Button buttonTouchToAddPerson;
        private CurrentPeople currentPeopleVisible;
        private bool explicitRefresh = false;
        private string ImageHash;

        public VisionPage()
        {
            layout = new StackLayout();
            Label label = new Label();
            label.Text = "Person detect near your door.";
            label.FontSize = 20;
            label.TextColor = Color.White;
            layout.Children.Add(label);
            BackgroundImage = "logos2.png";
            visionImage = new Image();
            labelInKnown = new Label();
            labelInKnown.TextColor = Color.White;
            labelInKnown.FontSize = 36;
            buttonTouchToAddPerson = new Button();
            buttonTouchToAddPerson.IsVisible = false;
            buttonTouchToAddPerson.Text = "Identify";
            labelInKnown.IsVisible = false;
            visionImage.VerticalOptions = LayoutOptions.FillAndExpand;
            layout.Children.Add(visionImage);
            layout.Children.Add(labelInKnown);
            layout.Children.Add(buttonTouchToAddPerson);
            buttonTouchToAddPerson.Clicked += ButtonClicked;
            Content = layout;
            Task.Factory.StartNew(UIThread,
                CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        public async void UIThread()
        {
            int sensorCounter = 0;
            for (int i = 0; i < 100000; i++)
            {
                //IEnumerable<Xamathon.Models.Frame> frames = null;
                Xamathon.Models.CurrentPeople currentPeople = null;
                try
                {
                    FrameService frameService = new FrameService();
                    currentPeople =
                        await frameService.GetCurrentPeople(Guid.Parse("79DEF068-E253-4CE9-B9CB-5FA78F46FD6E"));
                        //await frameService.GetCurrentPeople(Guid.Parse("59E99FBA-EE86-4553-9114-54749ED93106"));
                    if (currentPeople == null)
                    {
                        await Task.Delay(1000);
                        continue;
                    }

                    if (explicitRefresh || currentPeopleVisible == null ||
                        !currentPeople.Persons.Select(p => p.Id).SequenceEqual(currentPeopleVisible.Persons.Select(p => p.Id)))
                    {
                        currentPeopleVisible = currentPeople;

                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await visionImage.FadeTo(0, 120);
                            if (currentPeople.Persons[0].IsKnown)
                            {
                                labelInKnown.IsVisible = true;
                                labelInKnown.Text = currentPeople.Persons[0].Name;
                                buttonTouchToAddPerson.IsVisible = false;
                            }
                            else
                            {
                                labelInKnown.Text = "Unknown"; 
                                buttonTouchToAddPerson.IsVisible = true;
                            }

                            visionImage.Source = Utils.Base64ToImageSource(currentPeople.EncodedFrame);
                            await visionImage.FadeTo(1, 120);
                        });

                        explicitRefresh = false;

                    }
                    await Task.Delay(1000);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString() + e.Message.ToString());
                }
            }
        }

        private async void ButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new AddPersonPage(currentPeopleVisible));
            explicitRefresh = true;
        }
    }
}
