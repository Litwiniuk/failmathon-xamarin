﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace Xamathon
{
    public class CarouselPageView : CarouselPage
    {
        public IEnumerable<Xamathon.Models.Device> devices;
        public IEnumerable<Xamathon.Models.GlobalSetting> globalSettings;

        public CarouselPageView(IEnumerable<Xamathon.Models.Device> passingDevices, IEnumerable<Xamathon.Models.GlobalSetting> globalSettings)
        {
            devices = passingDevices;
            // The root page of your application

            var padding = new Thickness(0, Device.OnPlatform(40, 40, 0), 0, 0);


            #region Page1
            /*var page1 = new ContentPage
            {
                Padding = padding,
             //   BackgroundImage = "bg1.png",
                
            };

            var content1 = new StackLayout
            {

            };

            page1.Content = content1;
            var label1 = new Label();
            label1.Text = "WAT WORKS1";
            content1.Children.Add(label1);
            page1.Content = content1;*/


            #endregion

            #region Page2
            //var page2 = new ContentPage
            //{
            //    Padding = padding,
            //  //  BackgroundImage = "bg2.png",
            //    Content = new StackLayout
            //    {

            //    }
            //};

            //var content2 = new StackLayout
            //{

            //};

            //page2.Content = content2;
            //var label2 = new Label();
            //label2.Text = "WAT WORKS2";
            //content2.Children.Add(label2);
            //page2.Content = content2;

            #endregion

            #region Page3
            var page3 = new ContentPage
            {
                Padding = padding,
               // BackgroundImage = "bg3.png",
                Content = new StackLayout
                {


                }
            };

            var content3 = new StackLayout
            {
                Padding = new Thickness(37, 350, 0, 0)
            };



            page3.Content = content3;
            var label3 = new Label();
            label3.Text = "WAT WORKS3";
            content3.Children.Add(label3);
            page3.Content = content3;

            #endregion

            //Children.Add(page1);
            Children.Add(new SettingsPage(globalSettings));
            Children.Add(new HousePage());
            Children.Add(new VisionPage());

         
        }

        private void ButtonClicked(object sender, EventArgs args)
        {

        }
    }
}
