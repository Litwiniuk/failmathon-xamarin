﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Models
{
    public class Device
    {
       public Guid Id { get; set; }
       public string Name { get; set; }
       public Capability Capabilities { get; set; }
    }
}
