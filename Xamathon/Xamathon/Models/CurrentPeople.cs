﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Models
{
    public class CurrentPeople
    {
        public Person[] Persons { get; set; }
        public string EncodedFrame { get; set; }
    }
}
