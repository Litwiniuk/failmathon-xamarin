﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Models
{
    public class Settings
    {
        public Guid DeviceId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class GlobalSetting
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public GlobalSetting(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }
    }

}
