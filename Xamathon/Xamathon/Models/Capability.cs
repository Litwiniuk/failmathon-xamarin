﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Models
{
    public enum Capability
    {
        Temperature = 0x1,
        Humidity = 0x2,
        Accelerometer = 0x4,
        MagneticField = 0x8,
        Camera = 0x10
    }
}
