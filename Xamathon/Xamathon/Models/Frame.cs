﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Models
{
    public class Frame
    {
        public Guid Id;
        public Guid DeviceId { get; set; }
        public string EncodedImage { get; set; }
        public DateTime Timestamp { get; set; }

    }
}
