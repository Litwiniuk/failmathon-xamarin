﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Models
{
    public class SensorData
    {
        public Guid Id { get; set; }
        public Guid DeviceId { get; set; }
        public SensorDataType DataType { get; set; }
        public string Data { get; set; }
        public DateTime Timestamp { get; set; } = DateTime.Now;
        public Device Device { get; set; }
    }

    public enum SensorDataType
    {
        InMotion,
        Temperature,
        Humidity
    }
}
