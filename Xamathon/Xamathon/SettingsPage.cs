﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamathon.Services;
using Xamarin.Forms;

namespace Xamathon
{
    public class SettingsPage : ContentPage
    {
        public Label label;
        public bool alarm;
        public bool vision;
        public bool areaInformation;
        public IEnumerable<Xamathon.Models.GlobalSetting> globalSettings;
        public SettingsPage(IEnumerable<Xamathon.Models.GlobalSetting> setsGlobalSettings)
        {
            globalSettings = setsGlobalSettings;

            SetSavedSettings(setsGlobalSettings);

            var layout = new StackLayout();

            /*layout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            layout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            layout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            layout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            layout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            layout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            */
            Switch switcherAlarm = new Switch();
            switcherAlarm.Toggled += switcherAlarm_Toggled;
            switcherAlarm.IsToggled = alarm;
            Label labelAlarm = new Label();
   
            layout.Children.Add(labelAlarm);
            layout.Children.Add(switcherAlarm);



            Switch switcherVision = new Switch();
            switcherVision.Toggled += switcherVision_Toggled;
            switcherVision.IsToggled = vision;
            Label labelVision = new Label();
  


            layout.Children.Add(labelVision);
            layout.Children.Add(switcherVision);


            Switch switcherAreaInformation = new Switch();
            switcherAreaInformation.Toggled += switcherAreaInformation_Toggled;
            switcherAreaInformation.IsToggled = areaInformation;
            Label labelAreaInformation = new Label();


            layout.Children.Add(labelAreaInformation);
            layout.Children.Add(switcherAreaInformation);
            BackgroundImage = "logos3.png";
            Button button = new Button();
            button.Margin = new Thickness(0,290,0,0);
            //button.Text = "Save settings";
            button.BackgroundColor = Color.Transparent;
            button.Clicked += SaveSettingsClicked;

            layout.Children.Add(button);

            Content = layout;
        }

        private void SetSavedSettings(IEnumerable<Models.GlobalSetting> setsGlobalSettings)
        {
            if (setsGlobalSettings != null && setsGlobalSettings.Count() != 0)
            {
                Models.GlobalSetting alarmObj = setsGlobalSettings.FirstOrDefault(x => x.Name == "Alarm");
                if (alarmObj != null)
                {
                    alarm = Convert.ToBoolean(alarmObj.Value);
                }

                Models.GlobalSetting visionObj = setsGlobalSettings.FirstOrDefault(x => x.Name == "Door vision");
                if (visionObj != null)
                {
                    vision = Convert.ToBoolean(visionObj.Value);
                }

                Models.GlobalSetting areaInformationObj = setsGlobalSettings.FirstOrDefault(x => x.Name == "Environmental information");
                if (areaInformationObj != null)
                {
                    areaInformation = Convert.ToBoolean(areaInformationObj.Value);
                }
            }
        }

        void switcherAlarm_Toggled(object sender, ToggledEventArgs e)
        {
            alarm = e.Value;
        }

        void switcherVision_Toggled(object sender, ToggledEventArgs e)
        {
            vision = e.Value;
        }

        void switcherAreaInformation_Toggled(object sender, ToggledEventArgs e)
        {
            areaInformation = e.Value;
            
        }

        private void SaveSettingsClicked(object sender, EventArgs args)
        {
            SettingsService settingsService = new SettingsService();
            settingsService.SetSettings(new Models.GlobalSetting("Alarm", alarm.ToString()));
            settingsService.SetSettings(new Models.GlobalSetting("Door vision", vision.ToString()));
            settingsService.SetSettings(new Models.GlobalSetting("Environmental information", areaInformation.ToString()));
        }
    }
}
