﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Xamathon
{
    public static class ApiHelper
    {
        /// <summary>
        /// Calling api
        /// </summary>
        /// <typeparam name="T">Class</typeparam>
        /// <param name="client"></param>
        /// <param name="requestUri"></param>
        /// <param name="method"></param>
        /// <param name="obj"></param>
        /// <exception cref="HttpException"></exception>
        /// <returns>Json result</returns>
        public static async Task<T> CallApi<T>(HttpClient client, string requestUri = "", string method = "GET", object obj = null)
        {
            HttpResponseMessage response;

            if (method == "GET")
            {
                response = await client.GetAsync(requestUri).ConfigureAwait(false);
            }
            else if (method == "PUT" || method == "POST")
            {
                HttpContent content;
                var sendJson = JsonConvert.SerializeObject(obj).ToString();

                content = new StringContent(sendJson, Encoding.UTF8, "application/json");
                if (method == "PUT")
                {
                    response = await client.PutAsync(requestUri, content).ConfigureAwait(false);
                }
                else
                {
                    response = await client.PostAsync(requestUri, content).ConfigureAwait(false);
                }
            }
            else
            {
                response = new HttpResponseMessage();
            }


            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(response.StatusCode.ToString() + requestUri);
            }

            var json = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(json);
            return result;

        }

        public static async Task CallApi(HttpClient client, string requestUri = "", string method = "GET", object obj = null)
        {
            HttpResponseMessage response;
            if (method == "GET")
            {
                response = await client.GetAsync(requestUri);
            }
            else if (method == "PUT" || method == "POST")
            {
                HttpContent content;

                var sendJson = JsonConvert.SerializeObject(obj).ToString();

                content = new StringContent(sendJson, Encoding.UTF8, "application/json");
                if (method == "PUT")
                {
                    response = await client.PutAsync(requestUri, content);
                }
                else
                {
                    response = await client.PostAsync(requestUri, content);
                }
            }
            else
            {
                response = new HttpResponseMessage();
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(response.StatusCode.ToString() + requestUri);
            }

        }
    }
}
