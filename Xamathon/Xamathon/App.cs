﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamathon.Services;
using Xamarin.Forms;
using Xamathon.Models;
using System.Threading.Tasks;

namespace Xamathon
{
    public class App : Application
    {
         
        public App()
        {
            // The root page of your application

            var page = new ContentPage();
            var content = new StackLayout();
            page.Content = content;
            page.BackgroundImage = "logos.png";
            Button button = new Button();
            button.Clicked += ButtonClicked;
            button.BackgroundColor = Color.Transparent;
            
            //button.Text = "Get started with our home center!";
            button.Margin = new Thickness(50, 400, 50, 100);
            content.Children.Add(button);            

            MainPage = new NavigationPage(page);
        }

        private async void ButtonClicked(object sender, EventArgs args)
        {
            IEnumerable<Xamathon.Models.Device> devices = null;
            IEnumerable<Xamathon.Models.GlobalSetting> globalSettings = null;
            try
            {
                //devices = await GetAllDevice();
                globalSettings = await GetAllGlobalSettings();
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString() + e.Message.ToString());
            }

            App.Current.MainPage = new CarouselPageView(devices, globalSettings);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public async Task <IEnumerable<Xamathon.Models.Device>> GetAllDevice()
        {
            DevicesService deviceServices = new DevicesService();
            return await deviceServices.GetDevices();
        }

        public async Task<IEnumerable<Xamathon.Models.GlobalSetting>> GetAllGlobalSettings()
        {
            SettingsService settingsService = new SettingsService();
            return await settingsService.GetAllGlobalSettings();
        }
    }
}
