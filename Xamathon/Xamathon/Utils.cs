﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Xamathon
{
    public class Utils
    {
        public static ImageSource Base64ToImageSource(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image

            byte[] imageAsBytes = Convert.FromBase64String(base64String);
            return ImageSource.FromStream(() => new System.IO.MemoryStream(imageAsBytes));
        }
    }
}
