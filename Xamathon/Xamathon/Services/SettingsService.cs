﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamathon.Models;
using System.Threading.Tasks;
using System.Net.Http;

namespace Xamathon.Services
{
    public class SettingsService
    {
        public async Task<IEnumerable<Settings>> GetAllSettings()
        {
            HttpClient httpClient = HttpHelper.CreateHttpClient();
            return await ApiHelper.CallApi<IEnumerable<Settings>>(httpClient, "setting/", "GET");
        }

        public async void SetSettings(Settings setting)
        { 
            HttpClient httpClient = HttpHelper.CreateHttpClient("POST");
            await ApiHelper.CallApi(httpClient, "setting/Set", "POST", setting);
        }

        public async void SetSettings(GlobalSetting setting)
        {
            HttpClient httpClient = HttpHelper.CreateHttpClient("POST");
            await ApiHelper.CallApi(httpClient, "GlobalSettings/Set", "POST", setting);
        }

        public async Task<IEnumerable<GlobalSetting>> GetAllGlobalSettings()
        {
            HttpClient httpClient = HttpHelper.CreateHttpClient();
            return await ApiHelper.CallApi<IEnumerable<GlobalSetting>>(httpClient, "GlobalSettings/GetAll", "GET");
        }
    }
}
