﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamathon.Models;
using System.Threading.Tasks;
using System.Net.Http;

namespace Xamathon.Services
{
    public class DevicesService
    {
        private HttpClient httpClient = HttpHelper.CreateHttpClient();
        public async Task<IEnumerable<Device>> GetDevices()
        {
            return await ApiHelper.CallApi<IEnumerable<Device>>(httpClient, "device/get", "GET");
        }
    }
}
