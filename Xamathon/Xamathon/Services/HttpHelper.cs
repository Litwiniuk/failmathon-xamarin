﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamathon;

namespace Xamathon.Services
{
    public static class HttpHelper
    {
        public static string apiUrl = Constants.ApplicationURL;

        public static HttpClient CreateHttpClient(string type = "GET")
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(HttpHelper.apiUrl);

            httpClient.DefaultRequestHeaders.Add("ZUMO-API-VERSION", "2.0.0");
            if (type == "POST")
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            }
            
            return httpClient;
        }
    }
}
