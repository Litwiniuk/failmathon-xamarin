﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamathon.Models;
using System.Threading.Tasks;
using System.Net.Http;

namespace Xamathon.Services
{
    public class FrameService
    {
        private HttpClient httpClient = HttpHelper.CreateHttpClient();
        public async Task<IEnumerable<Frame>> GetFrames(DateTime time)
        {
            return await ApiHelper.CallApi<IEnumerable<Frame>>(httpClient, "frame/getall/" + time.Ticks , "GET").ConfigureAwait(false);
        }

        public async Task<Frame> GetLastFrame()
        {
            return await ApiHelper.CallApi<Frame>(httpClient, "frame/getlast/").ConfigureAwait(false);
        }

        public async Task<CurrentPeople> GetCurrentPeople(Guid deviceId)
        {
            return await ApiHelper.CallApi<CurrentPeople>(httpClient, "person/current/" + deviceId, "GET").ConfigureAwait(false);
        }
    }
}
