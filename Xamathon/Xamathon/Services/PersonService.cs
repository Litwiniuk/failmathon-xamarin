﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Xamathon.Services
{
    public class PersonService
    {
        public async Task SetName(PersonData data)
        {
            HttpClient httpClient = HttpHelper.CreateHttpClient("POST");
            await ApiHelper.CallApi(httpClient, "Person/SetName", "POST", data);
        }
    }

    public class PersonData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsAuthorized { get; set; }
    }
}
