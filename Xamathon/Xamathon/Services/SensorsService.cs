﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamathon.Models;
using System.Threading.Tasks;
using System.Net.Http;

namespace Xamathon.Services
{
    public class SensorsService
    {
        private HttpClient httpClient = HttpHelper.CreateHttpClient();
        public async Task<IEnumerable<SensorData>> GetSensorsData()
        {
            return await ApiHelper.CallApi<IEnumerable<SensorData>>(httpClient, "sensor/getdata", "GET");
        }
    }
}
