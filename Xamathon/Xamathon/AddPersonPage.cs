﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;
using Xamathon.Models;
using Xamathon.Services;

namespace Xamathon
{
    public class AddPersonPage : ContentPage
    {
        private CurrentPeople currentPeople;
        private Entry textEntry;
        private Switch switcher;
        public AddPersonPage(CurrentPeople people)
        {
            var layout = new StackLayout();
            currentPeople = people;
            BackgroundImage = "logos2.png";

            Image img = new Image();
            img.Source = Utils.Base64ToImageSource(currentPeople.EncodedFrame);
            layout.Children.Add(img);

            textEntry = new Entry();
            layout.Children.Add(textEntry);

            var label = new Label();
            label.Text = "Authorize";
            layout.Children.Add(label);

            switcher = new Switch();
            layout.Children.Add(switcher);

            var btn = new Button();
            btn.Text = "Save person name";
            btn.Clicked += BtnOnClicked;
            layout.Children.Add(btn);

            Content = layout;
        }

        private async void BtnOnClicked(object sender, EventArgs eventArgs)
        {
            var service = new PersonService();
            await service.SetName(new PersonData
            {
                Id = currentPeople.Persons[0].Id,
                Name = textEntry.Text,
                IsAuthorized = switcher.IsToggled
            });

            await Navigation.PopModalAsync();
        }
    }
}
